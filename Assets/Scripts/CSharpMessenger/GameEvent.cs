﻿public static class GameEvent
{
    public const string GameOverWin = "GameOverWin";
    public const string GameOverLose = "GameOverLose";
    public const string GameOverNoMatches = "GameOverNoMatches";
    public const string AddScore = "AddScore";
    public const string BoardInitialized = "BoardInitialized";
    public const string ValidMove = "ValidMove";
}