﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Gamepiece class.
/// </summary>
public class Piece : MonoBehaviour
{
    [SerializeField]
    private PieceType type;
    [SerializeField]
    private Sprite[] sprites;

    private int x;
    private int y;
    private bool isMoving = false;
    private Action<Piece, int, int> moveFinishedCallback;
    private int scoreValue;
    private SpriteRenderer spriteRenderer;

    public PieceType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public int X
    {
        get
        {
            return x;
        }
    }

    public int Y
    {
        get
        {
            return y;
        }
    }

    public int ScoreValue
    {
        get
        {
            return scoreValue;
        }
    }

    public bool HasColorBomb
    {
        get
        {
            Bomb attachedBomb = gameObject.GetComponentInChildren<Bomb>();
            return (attachedBomb != null && attachedBomb.BombType == BombType.Color);
        }
    }

    public void SetCoordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /// <summary>
    /// Set SpriteRenderer reference.
    /// </summary>
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnValidate()
    {
        SetSpriteToType();
    }

    /// <summary>
    /// Set the piece sprite according to its type.
    /// </summary>
    private void SetSpriteToType()
    {
        if (spriteRenderer != null)
        {
            spriteRenderer.sprite = sprites[(int)type];
        }
    }

    /// <summary>
    /// Initialize gamepiece. Set a callback to use when a piece has finished movement, set score value
    ///  and piece type.
    /// </summary>
    /// <param name="moveFinishedCallback">Callback to invoke after finished movement</param>
    /// <param name="scoreValue">Score value</param>
    /// <param name="type">Type of initialized piece</param>
    public void Init(Action<Piece, int, int> moveFinishedCallback, int scoreValue, PieceType type)
    {
        this.moveFinishedCallback = moveFinishedCallback;
        this.scoreValue = scoreValue;
        this.type = type;
        SetSpriteToType();
    }

    /// <summary>
    /// If the piece is not moving at the moment invoke MovementCoroutine() with the target coordinates,
    /// move time and finish callback.
    /// </summary>
    /// <param name="targetX">Move target x coordinate</param>
    /// <param name="targetY">Move target y coordinate</param>
    /// <param name="moveTime">Duration of movement</param>
    public void Move(int targetX, int targetY, float moveTime)
    {
        if (!isMoving)
        {
            StartCoroutine(MovementCoroutine(new Vector3(targetX, targetY, 0f), moveTime));
        }
    }

    /// <summary>
    /// Piece movement logic. While the piece is not near the destination point, lerp it towards its
    /// destination.
    /// </summary>
    /// <param name="destination">Destination of movement</param>
    /// <param name="moveTime">Duration of movement</param>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator MovementCoroutine(Vector3 destination, float moveTime)
    {
        Vector3 startPos = transform.position;
        bool hasArrived = false;
        float timeElapsed = 0f;
        isMoving = true;

        while (!hasArrived)
        {
            if (Vector3.Distance(transform.position, destination) < 0.01f)
            {
                hasArrived = true;
                moveFinishedCallback(this, (int)destination.x, (int)destination.y);
                break;
            }

            timeElapsed += Time.deltaTime;
            float t = Mathf.Clamp(timeElapsed / moveTime, 0f, 1f);

            // Smoothstep interpolation
            t = t * t * (3 - 2 * t);

            transform.position = Vector3.Lerp(startPos, destination, t);

            yield return null;
        }

        isMoving = false;
    }
}

public enum PieceType
{
    Blue,
    Green,
    Grey,
    Purple,
    Red,
    Yellow
}