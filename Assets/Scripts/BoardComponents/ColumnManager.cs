﻿using System.Collections.Generic;

/// <summary>
/// Manager of the piece slide/column collapse logic.
/// </summary>
public class ColumnManager
{
    private int width;
    private int height;
    private ArrayWrapper<Piece> pieces;
    private ArrayWrapper<Tile> tiles;
    private float slideSpeed = 0.01f;

    /// <summary>
    /// Ctor initializing board width and height fields, pieces and tiles ArrayWrappers.
    /// </summary>
    /// <param name="width">Width of board</param>
    /// <param name="height">Height of board</param>
    /// <param name="pieces">Piece ArrayWrapper</param>
    /// <param name="tiles">Tile ArrayWrapper</param>
    public ColumnManager(int width, int height, ArrayWrapper<Piece> pieces,
        ArrayWrapper<Tile> tiles)
    {
        this.width = width;
        this.height = height;
        this.pieces = pieces;
        this.tiles = tiles;
    }

    /// <summary>
    /// Get the indices of columns involved in the slide by invoking GetColumnIndices() on the input.
    /// Call SlideColumnAt() on every column index then return a collection of moved pieces.
    /// </summary>
    /// <param name="pieces">A collection of pieces</param>
    /// <returns>A HashSet of pieces that were moved during the slide</returns>
    public IEnumerable<Piece> SlideColumns(IEnumerable<Piece> pieces)
    {
        List<int> columnIndices = GetColumnIndices(pieces);
        HashSet<Piece> movedPieces = new HashSet<Piece>();

        foreach (int column in columnIndices)
        {
            movedPieces.UnionWith(SlideColumnAt(column));
        }

        return movedPieces;
    }

    /// <summary>
    /// Inspect the given column if an empty tile is found (and it is not an obstacle), find its
    /// closest upper neighbor and slide it down to the empty place. Repeat the process until no
    /// more pieces left to move.
    /// </summary>
    /// <param name="columnIndex">Index of the column to slide</param>
    /// <returns>A list of pieces involved in the slide</returns>
    private List<Piece> SlideColumnAt(int columnIndex)
    {
        List<Piece> piecesToMove = new List<Piece>();

        for (int i = 0; i < height - 1; i++)
        {
            if (pieces[columnIndex, i] == null && tiles[columnIndex, i].Type != TileType.Obstacle)
            {
                for (int j = i + 1; j < height; j++)
                {
                    if (pieces[columnIndex, j] != null)
                    {
                        pieces[columnIndex, j].Move(columnIndex, i, slideSpeed * (j - i));
                        pieces.SetElement(columnIndex, i, pieces[columnIndex, j]);
                        pieces[columnIndex, i].SetCoordinates(columnIndex, i);

                        if (!piecesToMove.Contains(pieces[columnIndex, i]))
                        {
                            piecesToMove.Add(pieces[columnIndex, i]);
                        }

                        pieces.SetElement(columnIndex, j, null);

                        break;
                    }
                }
            }
        }

        return piecesToMove;
    }

    /// <summary>
    /// Get column indices from a collection of Pieces.
    /// </summary>
    /// <param name="pieces">A collection of pieces</param>
    /// <returns>A list of column indices</returns>
    private List<int> GetColumnIndices(IEnumerable<Piece> pieces)
    {
        List<int> columnIndices = new List<int>();

        foreach (Piece piece in pieces)
        {
            if (piece!= null && !columnIndices.Contains(piece.X))
            {
                columnIndices.Add(piece.X);
            }
        }

        return columnIndices;
    }
}
