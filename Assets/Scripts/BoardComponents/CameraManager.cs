﻿using UnityEngine;

/// <summary>
/// Main camera manager class.
/// </summary>
public class CameraManager : MonoBehaviour
{
    /// <summary>
    /// Adjust camera size based on the play area width and height and play area border value.
    /// </summary>
    /// <param name="playAreaWidth">Play area width</param>
    /// <param name="playAreaHeight">Play area height</param>
    /// <param name="playAreaBorderSize">Play area border size</param>
    public void AdjustCamera(int playAreaWidth, int playAreaHeight, float playAreaBorderSize)
    {
        transform.position = new Vector3((playAreaWidth - 1) / 2f, (playAreaHeight - 1) / 2f, -10f);

        float aspect = Screen.width / (float)Screen.height;
        float verticalSize = playAreaHeight / 2f + playAreaBorderSize;
        float horizontalSize = (playAreaWidth / 2f + playAreaBorderSize) / aspect;

        gameObject.GetComponent<Camera>().orthographicSize =
            Mathf.Max(verticalSize, horizontalSize);
    }
}
