﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This class provide functionality to find matching pieces.
/// </summary>
public class MatchFinder
{
    private int boardWidth;
    private int boardHeight;
    private int boardMaxDimension;
    private int matchLength = 3;
    private int x;
    private int y;
    private ArrayWrapper<Piece> pieces;

    /// <summary>
    /// Ctor which initializes the board width and height fields, also the stores the bigger of the
    /// two in another field.
    /// </summary>
    /// <param name="boardWidth"></param>
    /// <param name="boardHeight"></param>
    public MatchFinder(int boardWidth, int boardHeight)
    {
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
        boardMaxDimension = Mathf.Max(boardWidth, boardHeight);
    }

    /// <summary>
    /// Find all the matching pieces in the given piece array wrapper.
    /// </summary>
    /// <param name="pieces">An ArrayWrapper containing pieces</param>
    /// <returns>A HashSet with all the matching pieces</returns>
    public IEnumerable<Piece> FindAllMatches(ArrayWrapper<Piece> pieces)
    {
        this.pieces = pieces;

        HashSet<Piece> allMatches = new HashSet<Piece>();

        for (int i = 0; i < boardWidth; i++)
        {
            for (int j = 0; j < boardHeight; j++)
            {
                allMatches.UnionWith(FindMatchesAt(i, j, null));
            }
        }

        return allMatches;
    }

    /// <summary>
    /// Call FindMatchInDirection() in vertical and horizontal direction starting at the given
    /// coordinates.
    /// </summary>
    /// <param name="x">X start coordinate</param>
    /// <param name="y">Y start coordinate</param>
    /// <param name="pieces">An ArrayWrapper containing pieces</param>
    /// <returns>A collection with all the matching pieces in both directions</returns>
    public IEnumerable<Piece> FindMatchesAt(int x, int y, ArrayWrapper<Piece> pieces)
    {
        this.x = x;
        this.y = y;

        if(pieces != null)
        {
            this.pieces = pieces;
        }

        // Horizontal plus vertical
        IEnumerable<Piece> allMatches = FindMatchInDirection(new Vector2(1, 0)).Union(
            FindMatchInDirection(new Vector2(0, 1)));

        return allMatches;
    }

    /// <summary>
    /// If the start piece is not null and not a color bomb, find all the matches in given direction
    /// by invoking CheckForMatch(). If the total count of the matches reaches the necessary limit,
    /// return the matching pieces.
    /// </summary>
    /// <param name="direction">A direction vector: vertical or horizontal</param>   
    /// <returns>A List containing all the valid matching pieces</returns>
    private List<Piece> FindMatchInDirection(Vector2 direction)
    {
        List<Piece> matches = new List<Piece>();
        Piece start = null;

        if (Board.IsWithinBoard(x, y))
        {
            start = pieces[x, y];
        }

        if (start != null && !start.HasColorBomb)
        {
            matches.Add(start);
        }
        else
        {
            return matches;
        }

        CheckForMatch(matches, start, direction);
        CheckForMatch(matches, start, -direction);

        if (matches.Count < matchLength)
        {
            matches.Clear();
        }

        return matches;
    }

    /// <summary>
    /// Find all the matching pieces in the given direction. If the indices are still within the board
    /// the next piece is checked for matching. If it has the same type, is not null and is not a 
    /// color bomb then it is added to the matches list.
    /// </summary>
    /// <param name="matches">A List of matching pieces</param>
    /// <param name="start">The start piece</param>
    /// <param name="direction">A direction vector: vertical or horizontal</param>
    private void CheckForMatch(List<Piece> matches, Piece start, Vector2 direction)
    {
        Vector2 startPos = new Vector2(start.X, start.Y);

        for (int i = 1; i < boardMaxDimension - 1; i++)
        {
            Vector2 coordinatesToCheck = startPos;
            coordinatesToCheck += direction * i;

            if (Board.IsWithinBoard((int)coordinatesToCheck.x, (int)coordinatesToCheck.y))
            {
                Piece nextPiece = pieces[(int)coordinatesToCheck.x, (int)coordinatesToCheck.y];

                if (nextPiece == null)
                {
                    break;
                }
                else
                {
                    if (nextPiece.Type == start.Type && !matches.Contains(nextPiece) &&
                        !nextPiece.HasColorBomb)
                    {
                        matches.Add(nextPiece);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                break;
            }
        }
    }
}
