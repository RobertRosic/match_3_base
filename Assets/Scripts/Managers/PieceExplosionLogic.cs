﻿using System.Collections;
using UnityEngine;

public class PieceExplosionLogic : MonoBehaviour
{
    private void OnEnable()
    {
        ParticleSystem particleSystem = GetComponent<ParticleSystem>();
        StartCoroutine(DisableAfterDuration(particleSystem.main.duration + 2f));
    }

    private IEnumerator DisableAfterDuration(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }
}
