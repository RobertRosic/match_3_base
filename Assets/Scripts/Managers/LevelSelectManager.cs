﻿using UnityEngine;

public class LevelSelectManager : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void OnLevelSelectClick(string level)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(level);
    }
}
