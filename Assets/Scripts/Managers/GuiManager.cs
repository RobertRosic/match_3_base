﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// GUI manager class.
/// </summary>
public class GuiManager : MonoBehaviour
{
    [SerializeField]
    private GameObject gui;

    private CanvasGroup gameOverGroup;
    private Dictionary<string, Text> guiTexts = new Dictionary<string, Text>();

    /// <summary>
    /// Find all text elements and create a dictionary indexed with their names. Disable/enable move
    /// and time counter as needed.
    /// </summary>
    /// <param name="isMoveLimitSet">Input from GameManager indicating if move limit is set</param>
    /// <param name="isTimeLimitSet">Input from GameManager indicating if time limit is set</param>
    public void Init(bool isMoveLimitSet, bool isTimeLimitSet)
    {
        gui = GameObject.Find("Gui");
        List<Text> foundTexts = new List<Text>();
        GameObject.Find("Gui").GetComponentsInChildren(foundTexts);

        foreach (Text text in foundTexts)
        {
            guiTexts.Add(text.name, text);
        }

        if (!isMoveLimitSet)
        {
            DisableTextElement("Move");
        }

        if (!isTimeLimitSet)
        {
            DisableTextElement("Time");
        }

        gameOverGroup = gui.GetComponentInChildren<CanvasGroup>();
    }

    /// <summary>
    /// Find a text element by name and disable it.
    /// </summary>
    /// <param name="name">Name of text element</param>
    private void DisableTextElement(string name)
    {
        foreach (KeyValuePair<string, Text> textElement in guiTexts.Where(t => t.Key.Contains(name)))
        {
            textElement.Value.enabled = false;
        }
    }

    /// <summary>
    /// Find a text element by name and set its value.
    /// </summary>
    /// <param name="name">Name of text element</param>
    /// <param name="value">Value of text element</param>
    public void SetTextValue(string name, string value)
    {
        guiTexts[name].text = value;
    }

    /// <summary>
    /// Add event handlers for game over events.
    /// </summary>
    private void OnEnable()
    {
        Messenger<string, string>.AddListener(GameEvent.GameOverWin, OnGameOver);
        Messenger<string, string>.AddListener(GameEvent.GameOverLose, OnGameOver);
        Messenger<string, string>.AddListener(GameEvent.GameOverNoMatches, OnGameOver);
    }

    /// <summary>
    /// Remove event handlers for game over events.
    /// </summary>
    private void OnDisable()
    {
        Messenger<string, string>.RemoveListener(GameEvent.GameOverWin, OnGameOver);
        Messenger<string, string>.RemoveListener(GameEvent.GameOverLose, OnGameOver);
        Messenger<string, string>.RemoveListener(GameEvent.GameOverNoMatches, OnGameOver);
    }

    /// <summary>
    /// Game over event handler. Update big and small text on game over panel, make the panel visible
    /// and interactable.
    /// </summary>
    /// <param name="bigText">Big text of game over panel</param>
    /// <param name="smallText">Small text of game over panel</param>
    private void OnGameOver(string bigText, string smallText)
    {
        SetTextValue("BigGameOverText", bigText);
        SetTextValue("SmallGameOverText", smallText);
        gameOverGroup.alpha = 1f;
        gameOverGroup.interactable = true;
    }    
}
