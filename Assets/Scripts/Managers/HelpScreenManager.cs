﻿using UnityEngine;

public class HelpScreenManager : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
        }
    }

    public void OnLevelSelectClick(string level)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(level);
    }
}
