﻿using UnityEngine;

/// <summary>
/// Game manager controlling game flow, score calculation, game over conditions, GUI updates.
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private bool isScoreLimitSet = false;
    [SerializeField]
    private bool isMoveLimitSet = false;
    [SerializeField]
    private bool isTimeLimitSet = false;
    [SerializeField]
    private int score = 0;
    [SerializeField]
    private int scoreLimit = 0;
    [SerializeField]
    private float remainingTime = 0f;
    [SerializeField]
    private GuiManager guiManager;
    [SerializeField]
    private int remainingMoves;

    private bool isBoardInitialized = false;
    private bool isTimeUp = false;

    /// <summary>
    /// Initialize GuiManager.
    /// </summary>
    private void Start()
    {
        guiManager = gameObject.AddComponent<GuiManager>();
        guiManager.Init(isMoveLimitSet, isTimeLimitSet);
        guiManager.SetTextValue("ScoreText", "0");

        if (isMoveLimitSet)
        {
            guiManager.SetTextValue("MovesText", remainingMoves.ToString());
        }

        if (isTimeLimitSet)
        {
            guiManager.SetTextValue("TimeText", TimeToReadableFormat(remainingTime));
        }
    }

    /// <summary>
    /// Handle time limit and escape key event.
    /// </summary>
    private void Update()
    {
        if (isTimeLimitSet)
        {
            remainingTime -= Time.deltaTime;

            if (remainingTime <= 0f)
            {
                isTimeUp = true;
                Messenger<string, string>.Broadcast(GameEvent.GameOverLose,
                        "You Lose", "You have run out of time!");
                guiManager.SetTextValue("TimeText", TimeToReadableFormat(0f));
                isTimeLimitSet = false;                
            }
            else
            {
                guiManager.SetTextValue("TimeText", TimeToReadableFormat(remainingTime));
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
        }
    }

    /// <summary>
    /// Event handler for home button. Return to level selection screen.
    /// </summary>
    public void OnHomeButtonClick()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
    }

    /// <summary>
    /// Reset score field and GUI text.
    /// </summary>
    public void ResetScore()
    {
        score = 0;
        guiManager.SetTextValue("ScoreText", score.ToString());
    }

    /// <summary>
    /// Add event handlers for messages.
    /// </summary>
    private void OnEnable()
    {
        Messenger<int>.AddListener(GameEvent.AddScore, OnAddScore);
        Messenger.AddListener(GameEvent.BoardInitialized, OnBoardInitialized);
        Messenger.AddListener(GameEvent.ValidMove, OnValidMove);
        Messenger<string, string>.AddListener(GameEvent.GameOverNoMatches, OnNoMatches);
    }

    /// <summary>
    /// Remove event handlers for messages.
    /// </summary>
    private void OnDisable()
    {
        Messenger<int>.RemoveListener(GameEvent.AddScore, OnAddScore);
        Messenger.RemoveListener(GameEvent.BoardInitialized, OnBoardInitialized);
        Messenger.RemoveListener(GameEvent.ValidMove, OnValidMove);
        Messenger<string, string>.RemoveListener(GameEvent.GameOverNoMatches, OnNoMatches);

    }

    /// <summary>
    /// Add score event handler. When score has increased update GUI and check if a target score
    /// has been reached.
    /// </summary>
    /// <param name="value">Score value</param>
    private void OnAddScore(int value)
    {
        if (isBoardInitialized && !isTimeUp)
        {
            score += value;
            guiManager.SetTextValue("ScoreText", score.ToString());

            if (isScoreLimitSet && score >= scoreLimit)
            {
                Messenger<string, string>.Broadcast(GameEvent.GameOverWin,
                    "You Win", "You have reached the target score!");
                isTimeLimitSet = false;
                isMoveLimitSet = false;
            }
        }
    }

    /// <summary>
    /// Event handler for board initialized message. Set flag to true.
    /// </summary>
    private void OnBoardInitialized()
    {
        isBoardInitialized = true;
    }

    /// <summary>
    /// Valid move event handler. Update GUI and check if the move limit has been reached.
    /// </summary>
    private void OnValidMove()
    {
        if (isMoveLimitSet)
        {
            remainingMoves--;
            guiManager.SetTextValue("MovesText", remainingMoves.ToString());

            if (remainingMoves == 0)
            {
                Messenger<string, string>.Broadcast(GameEvent.GameOverLose,
                    "You Lose", "You have no moves left!");
                isTimeLimitSet = false;
            }
        }
    }

    /// <summary>
    /// Helper method to convert time float value to a more readable format.
    /// </summary>
    /// <param name="time">Time in seconds</param>
    /// <returns>A formatted time string (mm:ss)</returns>
    private string TimeToReadableFormat(float time)
    {
        int minutes = Mathf.FloorToInt(time / 60f);
        int seconds = Mathf.FloorToInt(time - minutes * 60);
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    /// <summary>
    /// No more valid matches event handler. Disable the counting of remaining time.
    /// </summary>
    /// <param name="arg1">Unused first arg</param>
    /// <param name="arg2">Unused second arg</param>
    private void OnNoMatches(string arg1, string arg2)
    {
        isTimeLimitSet = false;
    }
}
