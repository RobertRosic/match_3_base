﻿using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    [SerializeField]
    private GameObject pieceExplosionPrefab;
    [SerializeField]
    private bool isEnabled = false;

    public bool IsEnabled
    {
        get
        {
            return isEnabled;
        }

        set
        {
            isEnabled = value;
        }
    }

    public void CreateExplosion(int x, int y, Color color)
    {
        if (isEnabled)
        {
            GameObject explosion = ObjectPoolManager.Current.GetPooledObject(pieceExplosionPrefab);
            explosion.transform.position = new Vector3(x, y, 0f);
            ParticleSystem particleSystem = explosion.GetComponent<ParticleSystem>();
            ParticleSystem.MainModule main = particleSystem.main;

            main.startColor = new ParticleSystem.MinMaxGradient(color);
            explosion.SetActive(true);
            particleSystem.Play();
        }
    }
}
