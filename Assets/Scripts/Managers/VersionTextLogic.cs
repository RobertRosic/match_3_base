﻿using UnityEngine;
using UnityEngine.UI;

public class VersionTextLogic : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = Application.version;
    }
}
