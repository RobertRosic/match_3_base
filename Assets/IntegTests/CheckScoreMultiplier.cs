﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckScoreMultiplier : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;
    private GameManager gameManager;
    StringIntDictionary scoreValues;
    Dictionary<string, int> scoreValuesDict;

    [SerializeField]
    private Text scoreText;

    private void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();

        scoreValues = gameObject.GetComponent<PieceManager>().GetType().
            GetField("scoreValues", BindingFlags.Instance | BindingFlags.NonPublic |
            BindingFlags.Public).GetValue(gameObject.GetComponent<PieceManager>()) as StringIntDictionary;

        scoreValuesDict = scoreValues.dictionary;

        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        bith.AutoTester.SetPieceRefill(false);
        yield return StartCoroutine(SimpleSwitchCollapse());
    }

    private IEnumerator SimpleSwitchCollapse()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(1, 0, 3);
        bith.AutoTester.SetPieceAt(3, 0, 4);
        bith.AutoTester.SetPieceAt(4, 0, 4);

        PieceType testPieceType1 = bith.PieceArrayWrapper[0, 0].Type;
        PieceType testPieceType2 = bith.PieceArrayWrapper[3, 0].Type;

        gameManager.ResetScore();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (
            scoreText.text !=
            ((scoreValuesDict[testPieceType1.ToString()] * 3 +
            scoreValuesDict[testPieceType2.ToString()] * 3 * 2)
            ).ToString()
           )
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for match 3 single collapse.");
        }

        yield return StartCoroutine(SimpleSwitchDoubleCollapse());
    }

    private IEnumerator SimpleSwitchDoubleCollapse()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(1, 0, 3);
        bith.AutoTester.SetPieceAt(3, 0, 4);
        bith.AutoTester.SetPieceAt(4, 0, 4);
        bith.AutoTester.SetPieceAt(4, 1, 1);
        bith.AutoTester.SetPieceAt(5, 0, 1);
        bith.AutoTester.SetPieceAt(6, 0, 1);

        PieceType testPieceType1 = bith.PieceArrayWrapper[0, 0].Type;
        PieceType testPieceType2 = bith.PieceArrayWrapper[3, 0].Type;
        PieceType testPieceType3 = bith.PieceArrayWrapper[4, 1].Type;

        gameManager.ResetScore();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (
            scoreText.text !=
            ((scoreValuesDict[testPieceType1.ToString()] * 3 +
            scoreValuesDict[testPieceType2.ToString()] * 3 * 2 +
            scoreValuesDict[testPieceType3.ToString()] * 3 * 3)
            ).ToString()
           )
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for match 3 double collapse.");
        }

        yield return StartCoroutine(SimpleSwitchDoubleCollapseWithBomb());
    }

    private IEnumerator SimpleSwitchDoubleCollapseWithBomb()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(1, 0, 3);
        bith.AutoTester.SetPieceAt(3, 0, 4);
        bith.AutoTester.SetPieceAt(4, 0, 4);
        bith.AutoTester.SetPieceAt(4, 1, 1);
        bith.AutoTester.SetPieceAt(5, 0, 1);
        bith.AutoTester.SetPieceAt(6, 0, 1);
        bith.AutoTester.SetPieceAt(4, 2, 5);
        bith.AutoTester.SetPieceAt(4, 3, 4);
        bith.AutoTester.SetPieceAt(4, 4, 5);
        bith.AutoTester.SetPieceAt(4, 5, 4);

        PieceType testPieceType1 = bith.PieceArrayWrapper[0, 0].Type;
        PieceType testPieceType2 = bith.PieceArrayWrapper[3, 0].Type;
        PieceType testPieceType3 = bith.PieceArrayWrapper[4, 1].Type;

        bith.AutoTester.PieceManager.AttachBomb(4, 1, BombType.Column);

        gameManager.ResetScore();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (
            scoreText.text !=
            ((scoreValuesDict[testPieceType1.ToString()] * 3 +
            scoreValuesDict[testPieceType2.ToString()] * 3 * 2 +
            (scoreValuesDict[testPieceType3.ToString()] * 3 +
            (scoreValuesDict[BombType.Column.ToString()]) +
            scoreValuesDict[testPieceType1.ToString()] * 2 +
            scoreValuesDict[testPieceType2.ToString()] * 2) * 3)
            ).ToString()
           )
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for match 3 double collapse with bomb.");
        }

        IntegrationTest.Pass();
    }
}
