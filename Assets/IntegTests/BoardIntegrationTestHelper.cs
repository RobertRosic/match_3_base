﻿using System.Collections;
using System.Reflection;
using UnityEngine;

public class BoardIntegrationTestHelper : MonoBehaviour
{
    private Board board;
    AutoTester autoTester;
    private ArrayWrapper<Piece> pieceArrayWrapper;
    private float waitTime;
    private bool isInitialized = false;

    public Board Board
    {
        get
        {
            return board;
        }
    }

    public AutoTester AutoTester
    {
        get
        {
            return autoTester;
        }
    }

    public ArrayWrapper<Piece> PieceArrayWrapper
    {
        get
        {
            return pieceArrayWrapper;
        }
    }

    public float WaitTime
    {
        get
        {
            return waitTime;
        }
    }

    public bool IsInitialized
    {
        get
        {
            return isInitialized;
        }
    }

    public static string MatchingPieceString(bool isMatchingPieceMoved)
    {
        return isMatchingPieceMoved ? "Matching piece moved." : "Non-matching piece moved.";
    }

    private void Start()
    {
        board = gameObject.GetComponent<Board>();
        StartCoroutine(Initialize());
    }

    private IEnumerator Initialize()
    {
        while (board.AutoTester == null)
        {
            yield return null;
        }

        autoTester = board.AutoTester;
        PieceSwitcher pieceSwitcher = board.GetComponent<PieceSwitcher>();

        FieldInfo switchTime = pieceSwitcher.GetType().GetField("switchSpeed",
         BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        waitTime = (float)switchTime.GetValue(pieceSwitcher);

        pieceArrayWrapper = autoTester.PieceManager.PieceArrayWrapper;

        isInitialized = true;
    }
}
