﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckBombExplosion : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        yield return StartCoroutine(ExplodeBlockBomb());
    }

    private IEnumerator ExplodeBlockBomb()
    {
        yield return StartCoroutine(ExplodeBlockBombLogic(0, 0, 0, 1, true));
        yield return StartCoroutine(ExplodeBlockBombLogic(0, 1, 0, 0, false));
        yield return StartCoroutine(ExplodeColumnBomb());
    }

    private IEnumerator ExplodeBlockBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(1, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        bith.AutoTester.PieceManager.AttachBomb(1, 1, BombType.Block);

        int[,] instanceIds = new int[3, 3];

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds.GetLength(1); j++)
            {
                instanceIds[i, j] = bith.PieceArrayWrapper[i, j].GetInstanceID();
            }
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds.GetLength(1); j++)
            {
                if (instanceIds[i, j] == bith.PieceArrayWrapper[i, j].GetInstanceID())
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces weren't removed after an explosion (block bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
                }
            }
        }

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] == null)
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Unfilled pieces left after an explosion (block bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
                }
            }
        }
    }

    private IEnumerator ExplodeColumnBomb()
    {
        yield return StartCoroutine(ExplodeColumnBombLogic(0, 0, 1, 0, true));
        yield return StartCoroutine(ExplodeColumnBombLogic(1, 0, 0, 0, false));
        yield return StartCoroutine(ExplodeRowBomb());
    }

    private IEnumerator ExplodeColumnBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(1, 1, 4);
        bith.AutoTester.SetPieceAt(1, 2, 4);

        bith.AutoTester.PieceManager.AttachBomb(1, 1, BombType.Column);

        int[] instanceIds = new int[bith.PieceArrayWrapper.GetLength(1)];

        for (int i = 0; i < instanceIds.Length; i++)
        {
            instanceIds[i] = bith.PieceArrayWrapper[1, i].GetInstanceID();
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            if (instanceIds[i] == bith.PieceArrayWrapper[1, i].GetInstanceID())
            {
                IntegrationTest.Fail(transform.parent.gameObject,
                    "Some pieces weren't removed after an explosion (column bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
            }
        }

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] == null)
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Unfilled pieces left after an explosion (column bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
                }
            }
        }
    }

    private IEnumerator ExplodeRowBomb()
    {
        yield return StartCoroutine(ExplodeRowBombLogic(0, 0, 0, 1, true));
        yield return StartCoroutine(ExplodeRowBombLogic(0, 1, 0, 0, false));
        yield return StartCoroutine(ExplodeColorBomb());
    }

    private IEnumerator ExplodeRowBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(1, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        bith.AutoTester.PieceManager.AttachBomb(1, 1, BombType.Row);

        int[] instanceIds = new int[bith.PieceArrayWrapper.GetLength(0)];

        for (int i = 0; i < instanceIds.Length; i++)
        {
            instanceIds[i] = bith.PieceArrayWrapper[i, 1].GetInstanceID();
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIds.Length; i++)
        {
            if (instanceIds[i] == bith.PieceArrayWrapper[i, 1].GetInstanceID())
            {
                IntegrationTest.Fail(transform.parent.gameObject,
                    "Some pieces weren't removed after an explosion (row bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
            }
        }

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] == null)
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Unfilled pieces left after an explosion (row bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
                }
            }
        }
    }

    private IEnumerator ExplodeColorBomb()
    {
        yield return StartCoroutine(ExplodeColorBombLogic(0, 0, 1, 0, true));
        yield return StartCoroutine(ExplodeColorBombLogic(1, 0, 0, 0, false));
        yield return StartCoroutine(ExplodeMultipleBombs());
    }

    private IEnumerator ExplodeColorBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(1, 0, 5);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);
        bith.AutoTester.PieceManager.AttachBomb(1, 0, BombType.Color);

        int[,] instanceIds = new int[8, 8];

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds.GetLength(1); j++)
            {
                instanceIds[i, j] = bith.PieceArrayWrapper[i, j].GetInstanceID();
            }
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds.GetLength(1); j++)
            {
                if (instanceIds[i, j] == bith.PieceArrayWrapper[i, j].GetInstanceID())
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces weren't removed after an explosion (color bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
                }
            }
        }

        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(1, 0, 5);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);
        PieceType removedType = bith.PieceArrayWrapper[1, 0].Type;

        int[,] instanceIds2 = new int[8, 8];

        for (int i = 0; i < instanceIds2.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds2.GetLength(1); j++)
            {
                instanceIds2[i, j] = bith.PieceArrayWrapper[i, j].GetInstanceID();
            }
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIds2.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds2.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j].Type == removedType &&
                    instanceIds2[i, j] == bith.PieceArrayWrapper[i, j].GetInstanceID())
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces of the exploded color weren't removed after an explosion (color bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
                }
            }
        }

        if (bith.PieceArrayWrapper[0, 0].GetInstanceID() == instanceIds2[0, 0])
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                        "Bomb piece wasn't removed after an explosion (color bomb). " +
                        BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }
    }

    private IEnumerator ExplodeMultipleBombs()
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(1, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        bith.AutoTester.PieceManager.AttachBomb(1, 1, BombType.Block);
        bith.AutoTester.PieceManager.AttachBomb(2, 1, BombType.Row);
        bith.AutoTester.PieceManager.AttachBomb(5, 1, BombType.Column);

        int[,] instanceIdsBlock = new int[3, 3];

        for (int i = 0; i < instanceIdsBlock.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIdsBlock.GetLength(1); j++)
            {
                instanceIdsBlock[i, j] = bith.PieceArrayWrapper[i, j].GetInstanceID();
            }
        }

        int[] instanceIdsColumn = new int[bith.PieceArrayWrapper.GetLength(1)];

        for (int i = 0; i < instanceIdsColumn.Length; i++)
        {
            instanceIdsColumn[i] = bith.PieceArrayWrapper[5, i].GetInstanceID();
        }

        int[] instanceIdsRow = new int[bith.PieceArrayWrapper.GetLength(0)];

        for (int i = 0; i < instanceIdsRow.Length; i++)
        {
            instanceIdsRow[i] = bith.PieceArrayWrapper[i, 1].GetInstanceID();
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(0, 0, 0, 1));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIdsBlock.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIdsBlock.GetLength(1); j++)
            {
                if (instanceIdsBlock[i, j] == bith.PieceArrayWrapper[i, j].GetInstanceID())
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces weren't removed after an explosion (multi bomb block).");
                }
            }
        }

        for (int i = 0; i < instanceIdsColumn.Length; i++)
        {
            if (instanceIdsColumn[i] == bith.PieceArrayWrapper[5, i].GetInstanceID())
            {
                IntegrationTest.Fail(transform.parent.gameObject,
                    "Some pieces weren't removed after an explosion (multi bomb column).");
            }
        }

        for (int i = 0; i < instanceIdsRow.Length; i++)
        {
            if (instanceIdsRow[i] == bith.PieceArrayWrapper[i, 1].GetInstanceID())
            {
                IntegrationTest.Fail(transform.parent.gameObject,
                    "Some pieces weren't removed after an explosion (multi bomb row).");
            }
        }

        IntegrationTest.Pass();
    }
}

