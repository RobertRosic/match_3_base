﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckColorBombIsPossibleMove : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        bith.AutoTester.SetPieceRefill(false);
        yield return StartCoroutine(RightNeighbor());
    }

    private IEnumerator RightNeighbor()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 1);
        bith.AutoTester.SetPieceAt(1, 1, 2);
        bith.AutoTester.SetPieceAt(1, 2, 3);
        bith.AutoTester.SetPieceAt(1, 3, 2);
        bith.AutoTester.SetPieceAt(1, 4, 2);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        yield return StartCoroutine(LeftNeighbor());
    }

    private IEnumerator LeftNeighbor()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 1);
        bith.AutoTester.SetPieceAt(3, 1, 2);
        bith.AutoTester.SetPieceAt(3, 2, 3);
        bith.AutoTester.SetPieceAt(3, 3, 2);
        bith.AutoTester.SetPieceAt(3, 4, 2);

        bith.AutoTester.PieceManager.AttachBomb(1, 0, BombType.Color);

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(3, 1, 3, 2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        yield return StartCoroutine(LowerNeighbor());
    }

    private IEnumerator LowerNeighbor()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(3, 0, 1);
        bith.AutoTester.SetPieceAt(3, 1, 2);
        bith.AutoTester.SetPieceAt(3, 2, 3);
        bith.AutoTester.SetPieceAt(3, 3, 2);
        bith.AutoTester.SetPieceAt(3, 4, 2);

        bith.AutoTester.PieceManager.AttachBomb(0, 1, BombType.Color);

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(3, 1, 3, 2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        yield return StartCoroutine(UpperNeighbor());
    }

    private IEnumerator UpperNeighbor()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(3, 0, 1);
        bith.AutoTester.SetPieceAt(3, 1, 2);
        bith.AutoTester.SetPieceAt(3, 2, 3);
        bith.AutoTester.SetPieceAt(3, 3, 2);
        bith.AutoTester.SetPieceAt(3, 4, 2);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(3, 1, 3, 2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }
    }
}

