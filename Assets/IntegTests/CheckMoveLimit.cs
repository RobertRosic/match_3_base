﻿using UnityEngine;

public class CheckMoveLimit : MonoBehaviour
{
    private bool moved = false;

    void Update()
    {
        if(!moved)
        {
            Messenger.Broadcast(GameEvent.ValidMove);
            Messenger.Broadcast(GameEvent.ValidMove);
            Messenger.Broadcast(GameEvent.ValidMove);
            moved = true;
        }        
    }
}
