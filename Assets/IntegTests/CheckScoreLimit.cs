﻿using UnityEngine;

public class CheckScoreLimit : MonoBehaviour
{
    private bool added = false;

    void Update()
    {
        if (!added)
        {
            Messenger<int>.Broadcast(GameEvent.AddScore, 50);
            Messenger<int>.Broadcast(GameEvent.AddScore, 150);
            added = true;
        }
    }
}
