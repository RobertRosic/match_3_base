﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckNoMoreMatchesMessage : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        bith.AutoTester.SetPieceRefill(false);
        yield return StartCoroutine(CreateNoMoreMatchState());
    }

    private IEnumerator CreateNoMoreMatchState()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 4);

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }
    }
}
